const mysql = require('mysql')

var connection = mysql.createPool({
    ///Properties ...
    connectionLimit: 150,
    host:'localhost',
    user: 'root',
    password: '',
    database: 'playtec'
});

module.exports = connection;
