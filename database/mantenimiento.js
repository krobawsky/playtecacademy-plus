var connection = require('./database')
var multer = require('multer')
var path = require('path')

function cursos_listado (req, res){
    connection.getConnection(function(error, tempCont){
            if(!!error){
                tempCont.release()
                console.log('Error')
            } else {
                //console.log('Conectado!')
                tempCont.query("SELECT usu.idUsuario id_usuario, usu.nombre nombre_usuario, cre.email email_usuario, cur.idCurso id_curso, cur.nombre nombre_curso, cur.descripcion descripcion_curso FROM usuario_has_curso usu_cur JOIN usuario usu ON usu_cur.usuario_idUsuario = usu.idUsuario JOIN curso cur ON usu_cur.curso_idCurso = cur.idCurso JOIN credencial cre ON usu.idUsuario = cre.Usuario_idUsuario WHERE cre.email = '"+req.session.email+"'", function(error, rows, fields){
                    tempCont.release()
                    if(!!error){
                        console.log('Error en la peticion')
                    } else {
                        //console.log(rows)
                        res.render('teacher/course_detail.ejs', {records: rows,
                                                                email: req.session.email,
                                                                apellido: req.session.apellido,
                                                                nombre: req.session.nombre})
                    }
                });
            }
    });
}
function clases_listado (req, res) {
    var xid = req.params.xid*1
    //About mysql ...
    connection.getConnection(function(error, tempCont){
    if(!!error){
        tempCont.release()
        console.log('Error')
    } else {
        //console.log('Conectado!')
        tempCont.query("SELECT cla.idclase id, cla.nombre nombre, cla.tiempo tiempo, cla.descripcion descripcion, cur.idCurso id_curso, cur.nombre nombre_curso, cla.imagen imagen FROM clase cla JOIN curso cur ON cla.curso_idCurso = cur.idCurso WHERE cur.idCurso = "+xid, function(error, rows, fields){
        tempCont.release()
    if(!!error){
        console.log('Error en la peticion')
    } else {
        //console.log(rows)
        res.render('teacher/class_detail.ejs', {records: rows})
                }
            });
        }
    });
}

module.exports = {
    cursos_listado: function (req, res) {
        if(req.session.nombre && req.session.rol=='P'){
           cursos_listado(req, res) 
        } else {
            res.redirect('/teacher/login/')
        }
    },
    clases_listado: function(req, res){
        if(req.session.nombre && req.session.rol=='P'){
           clases_listado(req, res)
        } else {
            res.redirect('/teacher/login/')
        }
    },
    //Capturar el pin ingresado del niño para mostrar ...
    diapositiva_actual: function(req, res){
        var xpin = req.query.pin;
        
        //Al ingresar como usuario se guardara en la sesion el PIN de ingreso
        req.session.pinAlumno = xpin;
        
        connection.getConnection(function(error, tempCont){
            if(!!error){
                tempCont.release()
                console.log('Error');
            } else {
                tempCont.query('SELECT idacceso, codigo, file FROM acceso WHERE codigo = "'+xpin+'"', function(error, rows, fields){
                    tempCont.release()
                    if(!!error){
                        console.log('Error en la petición');
                          res.send(error);
                    } else {
                       if(rows.length){
                            res.render('shared/presentation.ejs', {records: rows})
                        }else{
                            res.redirect('/');
                        }
                    }
                });
            }
        });
    },
    //Capturar el pin ingresado del niño para mostrar ...
    diapositivas_listado: function(req, res){
        
        var xpin = req.session.idClase;
        //Defined objects
        let usuariosConectados = {}
        let accesoClase = {}
        
        //Buscando el id del pin ...
        connection.getConnection(function(error, tempCont){
            if(!!error){
                tempCont.release()
                console.log('Error')
            } else {
                //console.log('Conectado!')
                tempCont.query("select idacceso, codigo from acceso where codigo = '"+req.session.codigoClase+"'", function(error, rows, fields){
                    tempCont.release()
                    if(!!error){
                        console.log('Error en la peticion: '+error)
                    } else {
                        accesoClase = rows
                    }
                });
            }
        });
        
        var idAcceso = accesoClase[0].idacceso
        //Filtrando alumnos por id del PIN
        connection.getConnection(function(error, tempCont){
            if(!!error){
                tempCont.release()
                console.log('Error')
            } else {
                //console.log('Conectado!')
                tempCont.query("SELECT * FROM usuarios_conectados WHERE acceso_idacceso = "+idAcceso, function(error, rows, fields){
                    tempCont.release()
                    if(!!error){
                        console.log('Error en la peticion: '+error)
                    } else {
                        usuariosConectados = rows
                    }
                });
            }
        });
        
        connection.getConnection(function(error, tempCont){
            if(!!error){
                tempCont.release()
                console.log('Error');
            } else {
                tempCont.query('SELECT iddiapositiva, file FROM diapositivas WHERE clase_idclase = "'+xpin+'"', function(error, rows, fields){
                    tempCont.release()
                    if(!!error){
                        console.log('Error en la petición');
                    } else {
                       if(rows.length){
                            res.render('teacher/command.ejs', {records: rows, codigoClase: req.session.codigoClase, usuariosConectados: usuariosConectados})
                        }else{
                            res.redirect('/');
                        }
                    }
                });
            }
        });
    },
    generando_codigo: function(req, res){
           
        var xid_curso = req.params['xid_curso']
        var xid_clase = req.params['xid_clase']
        
         function makeid() {
             var text = "";
             var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
             for (var i = 0; i < 6; i++){
                 text += possible.charAt(Math.floor(Math.random() * possible.length));
             }
             return text;
         }
        
        var codigoGenerado = makeid();
        
        //About mysql ...
        connection.getConnection(function(error, tempCont){
            if(!!error){
                tempCont.release()
                console.log('Error')
            } else {
                //console.log('Conectado!')
                tempCont.query("insert into acceso (codigo, clase_idclase, clase_curso_idCurso, file) values ('"+codigoGenerado+"', "+xid_clase+", "+xid_curso+", 'abc')", function(error, rows, fields){
                    tempCont.release()
                    if(!!error){
                        console.log('Error en la peticion: '+error)
                    } else {
                        //console.log(rows)
                        //res.render('teacher/code.ejs', {records: codigoGenerado})
                        req.session.codigoClase = codigoGenerado
                        req.session.idClase = xid_clase
                        res.redirect('/command/')
                    }
                });
            }
        });
    },
    diapo_actual: function(req, res){
        var codigo = req.body['codigo']
        var codigo2 = req.query.codigo
        connection.getConnection(function(error, tempCont){
            if(!!error){
                tempCont.release()
                console.log('Error')
            } else {
                tempCont.query("select file from acceso where codigo = '"+codigo2+"'", function(error, rows, fields){
                    tempCont.release()
                    if(!!error){
                        res.send("Error en la peticion: "+error)
                    } else {
                       res.send(rows[0].file)
                    }
                });
            }
        });
    },
    cambiar_diapo_actual: function(req, res){
        var codigo = req.body['codigo']
        var file = req.body['file']
        connection.getConnection(function(error, tempCont){
            if(!!error){
                tempCont.release()
                console.log('Error')
            } else {
                tempCont.query("update acceso set file = '"+file+"' where codigo = '"+codigo+"'", function(error, rows, fields){
                    tempCont.release()
                    if(!!error){
                        res.send("Error en la peticion: "+error)
                    } else {
                       res.send('Actualizado correctamente')
                    }
                });
            }
        });
    },
    validando_usuario: function(req, res){
        
        var email = req.body['xemail']
        var password = req.body['xpassword']
        
        //About mysql ...
        connection.getConnection(function(error, tempCont){
            if(!!error){
                tempCont.release()
                console.log('Error')
            } else {
                //console.log('Conectado!')
                tempCont.query("SELECT usu.idUsuario id, usu.nombre nombre, usu.apellido apellido, usu.rol rol, cre.email email FROM credencial cre JOIN usuario usu ON cre.Usuario_idUsuario = usu.idUsuario WHERE cre.email = '"+email+"' AND cre.password = '"+password+"'", function(error, rows, fields){
                    tempCont.release()
                    if(!!error){
                        res.send("Error en la peticion: "+error)
                    } else {
                        if (rows.length===0) {
                            res.render('teacher/login.ejs', {records_error: 'El usuario no existe'})
                        } else {
                            //Usuario existe (Guardando datos en la session con Express):
                            req.session.codigoUsuario = rows[0].id
                            req.session.apellido = rows[0].apellido
                            req.session.nombre = rows[0].nombre
                            req.session.email = rows[0].email
                            req.session.rol = rows[0].rol
                            
                            switch (req.session.rol) {
                                case 'A':
                                    res.redirect('/admin/')
                                    break;
                                case 'P':
                                    res.redirect('/teacher/course_detail/')
                                    break;
                                default:
                                    console.log('El usuario no tiene un rol válido')
                            }
                        }
                    }
                });
            }
        });
    },
    cerrar_sesion: function (req, res) {
        req.session.destroy()
        res.redirect('/teacher/login')
    },
    
    // ADMIN
    
    prof_listado: function (req, res) {
        if(req.session.nombre && req.session.rol=='A'){
            connection.getConnection(function(error, tempCont){
                if(!!error){
                    tempCont.release()
                    console.log('Error')
                } else {
                    //console.log('Conectado!')
                    tempCont.query("SELECT usu.idUsuario idUsuario, usu.nombre nombre, usu.apellido apellido, cre.email email "+
                    "FROM usuario usu JOIN credencial cre ON usu.idUsuario = cre.Usuario_idUsuario WHERE rol = 'P'", 
                    function(error, rows, fields){
                        tempCont.release()
                        if(!!error){
                            console.log('Error en la peticion')
                        } else {
                            //console.log(rows)
                            res.render('admin/profesores.ejs', {records: rows,
                                                                    idUsuario: req.session.idUsuario,
                                                                    email: req.session.email,
                                                                    apellido: req.session.apellido,
                                                                    nombre: req.session.nombre})
                        }
                    });
                }
            });
        
        } else {
            res.redirect('/teacher/login/')
        }
    },
    
    agregar_profesor: function (req, res) {
      if(req.session.nombre && req.session.rol=='A'){
        
        connection.getConnection(function(error, tempCont){
            if(!!error){
                tempCont.release()
                console.log('Error')
            } else {
                //console.log('Conectado!')
                
                var nombre = req.body.nombre
                var apellido = req.body.apellido
                var email = req.body.email
                
                tempCont.query("INSERT INTO usuario (nombre, apellido, rol) VALUES ('"+nombre+"', '"+apellido+"', 'P')",
                function(error, rows, fields){
                    tempCont.release()
                    if(!!error){
                        console.log('Error en la peticion')
                    } else {
                        console.log('Usuario creado: ' + rows.insertId)
                        tempCont.query("INSERT INTO credencial (Usuario_idUsuario, password, email) VALUES ('"+rows.insertId+"', 'playtec2019', '"+email+"')",
                        function(error, rows, fields){
                            if(!!error){
                                console.log('Error en la peticion')
                            } else {
                                console.log('Credenciales creadas')
                                res.redirect('profesores')
                            }
                        });
                    }
                });
            }
        });
      } else {
          res.redirect('/teacher/login/')
      }
    },
    
    mostrar_profesor: function (req, res) {
        
        connection.getConnection(function(error, tempCont){
            if(!!error){
                tempCont.release()
                console.log('Error')
            } else {
                //console.log('Conectado!')
                var xid = req.params.xid*1
                
                tempCont.query("SELECT usu.nombre nombre, usu.apellido apellido, cre.email email "+
                "FROM usuario usu JOIN credencial cre ON usu.idUsuario = cre.Usuario_idUsuario WHERE usu.idUsuario = '"+xid+"'", 
                function(error, rows, fields){
                    tempCont.release()
                    if(!!error){
                        console.log('Error en la peticion')
                    } else {
                        console.log(rows)
                        res.render('admin/editar_prof.ejs', {
                            record: rows[0]
                        })
                    }
                });
            }
        });
    },
    
    editar_profesor: function (req, res) {
        
        connection.getConnection(function(error, tempCont){
            if(!!error){
                tempCont.release()
                console.log('Error')
            } else {
                //console.log('Conectado!')
                var xid = req.params.xid*1
                
                var nombre = req.body.nombre
                var apellido = req.body.apellido
                var email = req.body.email
                
                tempCont.query("UPDATE usuario SET nombre = '"+nombre+"', apellido = '"+apellido+"' WHERE idUsuario = '"+xid+"' ",
                function(error, rows, fields){
                    tempCont.release()
                    if(!!error){
                        console.log('Error en la peticion')
                    } else {
                        console.log('Usuario editado: ' + xid)
                        tempCont.query("UPDATE credencial SET email = '"+email+"' WHERE Usuario_idUsuario = '"+xid+"' ",
                        function(error, rows, fields){
                            if(!!error){
                                console.log('Error en la peticion')
                            } else {
                                console.log('Credenciales editadas')
                                res.redirect('profesores')
                            }
                        });
                    }
                });
            }
        });
    },
    
    alum_listado: function (req, res) {
      
      if(req.session.nombre && req.session.rol=='A'){
         connection.getConnection(function(error, tempCont){
            if(!!error){
                tempCont.release()
                console.log('Error')
            } else {
                //console.log('Conectado!')
                tempCont.query("SELECT * FROM usuario WHERE rol = 'a'", function(error, rows, fields){
                    tempCont.release()
                    if(!!error){
                        console.log('Error en la peticion')
                    } else {
                        //console.log(rows)
                        res.render('admin/alumnos.ejs', {records: rows,
                                                                apellido: req.session.apellido,
                                                                nombre: req.session.nombre})
                    }
                });
            }
        });
      } else {
          res.redirect('/teacher/login/')
      }
    },
    
    curso_listado: function (req, res) {
      
      if(req.session.nombre && req.session.rol=='A'){
         connection.getConnection(function(error, tempCont){
            if(!!error){
                tempCont.release()
                console.log('Error')
            } else {
                //console.log('Conectado!')
                tempCont.query("SELECT * FROM curso", function(error, rows, fields){
                    tempCont.release()
                    if(!!error){
                        console.log('Error en la peticion')
                    } else {
                        //console.log(rows)
                        res.render('admin/cursos.ejs', {records: rows,
                                                                idCurso: req.session.idCurso,
                                                                nombre: req.session.nombre,
                                                                descripcion: req.session.descripcion})
                    }
                });
            }
        });
      } else {
          res.redirect('/teacher/login/')
      }
    },
    
    curso_clases: function (req, res) {
      
      if(req.session.nombre && req.session.rol=='A'){
        connection.getConnection(function(error, tempCont){
            if(!!error){
                tempCont.release()
                console.log('Error')
            } else {
                var xid = req.params.xid*1
                var nombre_curso = req.query.nombre_curso
                //console.log('Conectado!')
                tempCont.query("SELECT * FROM clase WHERE curso_idCurso='"+xid+"'", function(error, rows, fields){
                    tempCont.release()
                    if(!!error){
                        console.log('Error en la peticion')
                    } else {
                        //console.log(rows)
                        res.render('admin/clases.ejs', {records: rows,
                                                                idclase: req.session.idclase,
                                                                nombre: req.session.nombre,
                                                                descripcion: req.session.descripcion,
                                                                id_curso: xid,
                                                                nombre_curso: nombre_curso
                        })
                    }
                });
            }
        });
      } else {
          res.redirect('/teacher/login/')
      }
    },
    
    curso_clases_pres: function (req, res) {
      
      if(req.session.nombre && req.session.rol=='A'){
        connection.getConnection(function(error, tempCont){
            if(!!error){
                tempCont.release()
                console.log('Error')
            } else {
                var xid = req.params.xid*1
                var nombre_clase = req.query.nombre_clase
                var id_curso = req.query.id_curso
                var nombre_curso = req.query.nombre_curso
                //console.log('Conectado!')
                tempCont.query("SELECT * FROM diapositivas WHERE clase_idclase='"+xid+"'", function(error, rows, fields){
                    tempCont.release()
                    if(!!error){
                        console.log('Error en la peticion')
                    } else {
                        //console.log(rows)
                        res.render('admin/presentaciones.ejs', {records: rows,
                                                                iddiapositiva: req.session.iddiapositiva,
                                                                file: req.session.file,
                                                                id_clase: xid,
                                                                nombre_clase: nombre_clase,
                                                                id_curso: id_curso,
                                                                nombre_curso: nombre_curso,
                        })
                    }
                });
            }
        });
      } else {
          res.redirect('/teacher/login/')
      }
    },
    
    curso_nuevo: function (req, res) {
      if(req.session.nombre && req.session.rol=='A'){
        
        connection.getConnection(function(error, tempCont){
            if(!!error){
                tempCont.release()
                console.log('Error')
            } else {
                //console.log('Conectado!')
                
                var nombre = req.body.nombre
                var descripcion = req.body.descripcion
                var imagen = "image_default.png"
                
                tempCont.query("INSERT INTO curso (nombre, descripcion, imagen) VALUES ('"+nombre+"', '"+descripcion+"', '"+imagen+"')",
                function(error, rows, fields){
                    tempCont.release()
                    if(!!error){
                        console.log('Error en la peticion')
                    } else {
                        console.log('Curso creado: ' + rows.insertId)
                        tempCont.query("SELECT * FROM clase WHERE curso_idCurso='"+rows.insertId+"'", function(error, rows, fields){

                        if(!!error){
                            console.log('Error en la peticion')
                        } else {
                            //console.log(rows)
                            res.render('admin/clases.ejs', {records: rows,
                                                                    idclase: req.session.idclase,
                                                                    nombre: req.session.nombre,
                                                                    descripcion: req.session.descripcion,
                                                                    id_curso: rows.insertId,
                                                                    nombre_curso: nombre
                            })
                        }
                });
                    }
                });
            }
        });
      } else {
          res.redirect('/teacher/login/')
      }
    },
    
    clase_nuevo: function (req, res) {
      if(req.session.nombre && req.session.rol=='A'){
        
        connection.getConnection(function(error, tempCont){
            if(!!error){
                tempCont.release()
                console.log('Error')
            } else {
                //console.log('Conectado!')
                
                var id_curso = req.query.id_curso
                var nombre_curso = req.query.nombre_curso
                var nombre = req.body.nombre
                var descripcion = req.body.descripcion
                var imagen = "image_default.png"
                
                tempCont.query("INSERT INTO clase (nombre, descripcion, imagen, curso_idCurso) VALUES ('"+nombre+"', '"+descripcion+"', '"+imagen+"' , '"+id_curso+"')",
                function(error, rows, fields){
                    tempCont.release()
                    if(!!error){
                        console.log('Error en la peticion')
                    } else {
                        console.log('Clase creado: ' + rows.insertId)
                        var xid = rows.insertId
                        //console.log('Conectado!')
                        tempCont.query("SELECT * FROM diapositivas WHERE clase_idclase='"+xid+"'", function(error, rows, fields){
                            if(!!error){
                                console.log('Error en la peticion')
                            } else {
                                //console.log(rows)
                                res.render('admin/presentaciones.ejs', {records: rows,
                                                                        iddiapositiva: req.session.iddiapositiva,
                                                                        file: req.session.file,
                                                                        id_clase: xid,
                                                                        nombre_clase: nombre,
                                                                        id_curso: id_curso,
                                                                        nombre_curso: nombre_curso,
                                })
                            }
                        });
                    }
                });
            }
        });
      } else {
          res.redirect('/teacher/login/')
      }
    },
    
     diapositiva_nuevo: function (req, res) {
        
        var storage = multer.diskStorage({
            destination: function (req, file, callback) {
            callback(null, '../public/pptas/uploads/');
            },
            filename: function (req, file, callback) {
            callback(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
            }
        });
        
        var upload = multer({
            storage: storage,
            fileFilter: function (req, file, callback) {
                var ext = path.extname(file.originalname);
                if(ext !== '.ppt' && ext !== '.pptx') {
                return callback(new Error('Solo se permiten imagenes'));
        }
        
        callback(null, true);
            }
        }).single('ppta'); //<input type="file" name="ppta">

        upload(req,res,function(err) {
            if(err) {
                res.status(500).json({type: 'error',message: 'Errores vergas imagen'});
                return;
            }else{
    			
    		}
        });
 
     
    },

    registro_alumno: function (req, res) {
        
        var nombres = req.body['nombres']
        var apodo = req.body['apodo']
        
        /*Help from Jorge's*/
        //Obtener id's
        
        var pin = req.session.pinAlumno
        
        connection.getConnection(function(error, tempCont){
            if(!!error){
                tempCont.release()
                console.log('Error')
            } else {
                //console.log('Conectado!')
                tempCont.query("select idacceso, codigo, clase_idclase, clase_curso_idCurso, file from acceso where codigo = '"+pin+"'", function(error, rows, fields){
                    tempCont.release()
                    if(!!error){
                        console.log('Error en la peticion')
                    } else {
                        connection.getConnection(function(error, tempCont){
                            if(!!error){
                                tempCont.release()
                                console.log('Error')
                            } else {
                                //console.log('Conectado!')
                                tempCont.query("INSERT INTO usuarios_conectados (nombres, apodo, acceso_idacceso, acceso_clase_idclase, acceso_clase_curso_idCurso) VALUES ('"+nombres+"', '"+apodo+"', "+rows[0].idacceso+", "+rows[0].clase_idclase+", "+rows[0].clase_curso_idCurso+")", function(error, rows, fields){
                                    tempCont.release()
                                    if(!!error){
                                        console.log('Error en la peticion')
                                    } else {
                                        console.log('Usuario conectado')
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }
}