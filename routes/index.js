var express = require('express');
var router = express.Router();

var Converter = require('../public/javascripts/convert');
var glob = require('glob');

var multer = require('multer')
var path = require('path')

var storage = multer.diskStorage({
            destination: function (req, file, callback) {
            callback(null, './public/ppt/');
            },
            filename: function (req, file, callback) {
            callback(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
            }
        });

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Ppt Converter' });
  console.log('------------------------');
  glob('**/example.ppt', {}, function(error, files) {
    console.log('files: ', files.length);
    if(files) {
        new Converter({
            files:          files,
            output:         'public/output/imgs/',
            invert:         false,
            greyscale:      false,
            deletePdfFile:  false,
            outputType:     'png',
            logLevel:       2,
            fileNameFormat: '_vers_%d',
            callback:       function(data) {
                console.log(data.failed, data.success.length, data.files.length, data.time);
            }
        }).run();
    }
  });
});


router.post('/', function(req, res) {
    
    var upload = multer({
        storage: storage,
        fileFilter: function (req, file, callback) {
            console.log('file: '+file.originalname);
            var ext = path.extname(file.originalname);
            if(ext !== '.ppt' && ext !== '.pptx' && ext !== '.pdf') {
                return callback(new Error('Solo se permiten ppts'));
            }
            callback(null, true);
        }
    }).single('ppt'); //<input type="file" name="imagen">

    upload(req,res,function(err) {
        if(err) {
            res.status(500).json({type: 'error',message: 'Errores vergas imagen'});
            return;
        }else{
            var path = req.file.path
            var filename = req.file.filename
            var destination = req.file.destination
            console.log(filename)
            console.log(path)
            console.log(destination)
			res.render('index', { title: 'Ppt Converter' });
			console.log('------------------------');
            glob('**/'+filename, {}, function(error, files) {
                console.log('files: ', files.length);
                if(files) {
                    new Converter({
                        files:          files,
                        output:         'public/output/imgs/',
                        invert:         false,
                        callback:       function(data) {
                            console.log(data.failed, data.success.length, data.files.length, data.time);
                        }
                    }).run();
                }
            });
			
        }
    });
    
});

module.exports = router;
