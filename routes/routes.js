var express = require('express');
var router = express.Router();
var fmantenimiento = require('../database/mantenimiento')

//Inicio
router.get('/', (req, res, next)=>{
    res.render('index.ejs')
})
//Cursos
router.get('/teacher/course_detail/', fmantenimiento.cursos_listado)
//Clases
router.get('/teacher/class_detail/:xid/', fmantenimiento.clases_listado)
//Presentaciones
router.get('/presentation', fmantenimiento.diapositiva_actual)
//Presentaciones
router.get('/command/', fmantenimiento.diapositivas_listado)
//Codigo autogenerado
router.get('/shared/presentation/:xid_curso/:xid_clase/', fmantenimiento.generando_codigo)
//Login profesor
router.get('/teacher/login/', function(req, res){
    
    if(req.session.nombre && req.session.rol=='P'){
        res.redirect('/teacher/course_detail/')
    } 
    else if(req.session.nombre && req.session.rol=='A'){
        res.redirect('/admin')
    } else {
        res.render('teacher/login.ejs' , {records_error: ''})
    }
    
})
router.post('/teacher/login_validate/', fmantenimiento.validando_usuario)

router.get('/teacher/logout/', fmantenimiento.cerrar_sesion)

router.post('/cambiar_diapo_actual', fmantenimiento.cambiar_diapo_actual)

router.get('/diapo_actual', fmantenimiento.diapo_actual)

// ADMIN
router.get('/admin', (req, res)=>{
    if(req.session.nombre && req.session.rol=='A'){
        res.render('admin/dashboard.ejs')
    } else {
        res.render('teacher/login.ejs' , {records_error: ''})
    }
})

router.get('/profesores', fmantenimiento.prof_listado)

router.get('/nuevo_profesor', (req, res)=>{
    res.render('admin/nuevo_prof.ejs')
})
router.post('/agregar_profesor', fmantenimiento.agregar_profesor)

router.get('/editar_prof/:xid', fmantenimiento.mostrar_profesor)
router.post('/editar_prof/:xid', fmantenimiento.editar_profesor)

router.get('/alumnos', fmantenimiento.alum_listado)

router.get('/cursos', fmantenimiento.curso_listado)
router.get('/curso/:xid', fmantenimiento.curso_clases)
router.get('/curso/clase/:xid', fmantenimiento.curso_clases_pres)

router.get('/cursos/nuevo', (req, res)=>{
    res.render('admin/nuevo_curso.ejs')
})
router.post('/cursos/nuevo', fmantenimiento.curso_nuevo)

router.get('/clases/nuevo', (req, res)=>{
    var id_curso = req.query.id_curso
    var nombre_curso = req.query.nombre_curso
    res.render('admin/nuevo_clase.ejs', {id_curso: id_curso,
                                        nombre_curso: nombre_curso})
})
router.post('/clases/nuevo', fmantenimiento.clase_nuevo)

router.post('/diapositivas/nuevo', fmantenimiento.diapositiva_nuevo)

router.post('/presentation/register/', fmantenimiento.registro_alumno)

module.exports = router