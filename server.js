const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const path = require('path')
const cookieParser = require('cookie-parser');
const session = require('express-session');
var http = require('http').Server(app);
var io = require('socket.io')(http);

var createError = require('http-errors');
var logger = require('morgan');

//Manejando sesiones
app.use(session({
    secret: 'this is secret',
    resave: false,
    saveUninitialized: true,
    cookie: { maxAge: 3600000 }
}))

app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


// All routes
var routes = require('./routes/routes')
var index = require('./routes/index')

//Using routes
app.use('/', routes)
app.use('/ppt', index)

io.on('connection', (socket) => {
    console.log('a user connected');
    socket.on('disconnect', function(){
        console.log('Se fue con otro');
    });
    socket.on('change image', (data) => {
        socket.broadcast.emit('load image', {
          message: data
        });
    });
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render(err);
});


http.listen(8080, ()=>{
    console.log('Listen in port http://localhost:8080')
})