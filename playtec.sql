-- MySQL dump 10.13  Distrib 5.5.57, for debian-linux-gnu (x86_64)
--
-- Host: 0.0.0.0    Database: playtec
-- ------------------------------------------------------
-- Server version	5.5.57-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `acceso`
--

DROP TABLE IF EXISTS `acceso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acceso` (
  `idacceso` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` char(6) NOT NULL,
  `clase_idclase` int(11) NOT NULL,
  `clase_curso_idCurso` int(11) NOT NULL,
  `file` varchar(400) DEFAULT NULL,
  PRIMARY KEY (`idacceso`,`clase_idclase`,`clase_curso_idCurso`),
  KEY `fk_acceso_clase1_idx` (`clase_idclase`,`clase_curso_idCurso`),
  CONSTRAINT `fk_acceso_clase1` FOREIGN KEY (`clase_idclase`, `clase_curso_idCurso`) REFERENCES `clase` (`idclase`, `curso_idCurso`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acceso`
--

LOCK TABLES `acceso` WRITE;
/*!40000 ALTER TABLE `acceso` DISABLE KEYS */;
INSERT INTO `acceso` VALUES (1,'magic',1,1,'/images/uploads/robotica10.jpg'),(2,'AV55lf',3,2,'abc'),(3,'sQI3x8',1,1,'abc'),(4,'1bz0S5',2,2,'abc'),(5,'gMlCpx',3,2,'abc'),(6,'hJTQA6',2,2,'abc'),(7,'23bHKD',2,2,'abc'),(8,'1jIhzr',3,2,'abc'),(9,'55FvUi',1,1,'abc'),(10,'fF8Jxi',2,2,'abc'),(11,'P6sLSq',3,2,'abc'),(12,'3cWlrs',3,2,'abc'),(13,'rhePE5',3,2,'abc'),(14,'NMUKoF',2,2,'abc'),(15,'NSEcLW',3,2,'abc'),(16,'TITR2b',4,3,'abc'),(17,'PHcVnk',2,2,'abc'),(18,'iEgUeP',4,3,'abc'),(19,'e1rbkR',4,3,'abc'),(20,'2sXPIS',4,3,'abc'),(21,'vD9cB7',4,3,'abc'),(22,'nhmFDL',4,3,'abc'),(23,'aIq9pv',4,3,'abc'),(24,'wOMLGA',2,2,'abc'),(25,'dWFHzd',4,3,'abc'),(26,'1fxB66',4,3,'abc'),(27,'jwwSIn',4,3,'abc'),(28,'TuALgx',4,3,'abc'),(29,'iO3uvT',4,3,'abc'),(30,'NLmzqa',4,3,'abc'),(31,'mbScoZ',2,2,'abc'),(32,'38spui',4,3,'abc'),(33,'J9A0yW',4,3,'abc'),(34,'uweSyo',4,3,'abc'),(35,'WyVbjx',4,3,'abc'),(36,'6NCN4G',4,3,'https://playtec-web-4-jrgflores.c9users.io/images/uploads/robotica10.jpg'),(37,'NRS6IJ',4,3,'https://playtec-web-4-jrgflores.c9users.io/images/uploads/robotica1.jpg'),(38,'lIoOjO',4,3,'https://playtec-web-4-jrgflores.c9users.io/images/uploads/robotica1.jpg'),(39,'OsuVBL',4,3,'abc'),(40,'xtmJIc',4,3,'https://playtec-web-4-jrgflores.c9users.io/images/uploads/robotica1.jpg'),(41,'JXSUFV',4,3,'/images/uploads/robotica1.jpg'),(42,'LASXKE',4,3,'/images/uploads/robotica1.jpg'),(43,'CADGNM',4,3,'https://playtec-web-4-jrgflores.c9users.io/images/uploads/robotica2.jpg'),(44,'ICKQBN',4,3,'https://playtec-web-4-jrgflores.c9users.io/images/uploads/robotica1.jpg'),(45,'RUSVEL',4,3,'/images/uploads/robotica5.jpg'),(46,'CGGRIU',2,2,'abc'),(47,'IJHCTU',4,3,'https://playtec-web-4-jrgflores.c9users.io/images/uploads/robotica1.jpg'),(48,'XTZGUR',4,3,'https://playtec-web-4-jrgflores.c9users.io/images/uploads/robotica1.jpg'),(49,'QGZTEN',4,3,'https://playtec-web-4-jrgflores.c9users.io/images/uploads/robotica1.jpg'),(50,'SOAXBJ',4,3,'https://playtec-web-4-jrgflores.c9users.io/images/uploads/robotica1.jpg'),(51,'ZPWSVG',4,3,'/images/uploads/robotica1.jpg'),(52,'IGLESQ',4,3,'/images/uploads/robotica1.jpg'),(53,'NPWPUL',4,3,'/images/uploads/robotica1.jpg'),(54,'CFDAFE',2,2,'abc'),(55,'HEEUPY',4,3,'/images/uploads/robotica1.jpg'),(56,'PHTXIF',4,3,'https://playtec-web-4-jrgflores.c9users.io/images/uploads/robotica1.jpg'),(57,'FRYAWT',4,3,'https://playtec-web-4-jrgflores.c9users.io/images/uploads/robotica1.jpg'),(58,'JIPUEQ',4,3,'/images/uploads/robotica1.jpg'),(59,'MILEJY',4,3,'/images/uploads/robotica1.jpg'),(60,'KQRFIU',4,3,'/images/uploads/robotica1.jpg'),(61,'RDXKVZ',4,3,'https://playtec-web-4-jrgflores.c9users.io/images/uploads/robotica1.jpg'),(62,'JEUSWQ',4,3,'abc'),(63,'JUBNME',4,3,'abc'),(64,'SQXNJA',4,3,'https://playtec-web-4-jrgflores.c9users.io/images/uploads/robotica1.jpg'),(65,'AHMHNB',4,3,'abc'),(66,'BQAHZP',4,3,'/images/uploads/robotica1.jpg'),(67,'HNJUAX',4,3,'/images/uploads/robotica1.jpg'),(68,'ZPRRDM',4,3,'/images/uploads/robotica1.jpg'),(69,'WSTICQ',4,3,'https://playtec-web-4-jrgflores.c9users.io/images/uploads/robotica1.jpg'),(70,'AUPCIV',4,3,'/images/uploads/robotica1.jpg'),(71,'SKWWRP',4,3,'abc'),(72,'IXTXXS',4,3,'https://playtec-web-4-jrgflores.c9users.io/images/uploads/robotica1.jpg'),(73,'OAGKZU',4,3,'abc');
/*!40000 ALTER TABLE `acceso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clase`
--

DROP TABLE IF EXISTS `clase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clase` (
  `nombre` varchar(250) NOT NULL,
  `tiempo` varchar(250) DEFAULT NULL,
  `descripcion` varchar(500) NOT NULL,
  `idclase` int(11) NOT NULL AUTO_INCREMENT,
  `curso_idCurso` int(11) NOT NULL,
  `imagen` varchar(250) DEFAULT 'image_default.jpg',
  PRIMARY KEY (`idclase`,`curso_idCurso`),
  KEY `fk_clase_curso1_idx` (`curso_idCurso`),
  CONSTRAINT `fk_clase_curso1` FOREIGN KEY (`curso_idCurso`) REFERENCES `curso` (`idCurso`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clase`
--

LOCK TABLES `clase` WRITE;
/*!40000 ALTER TABLE `clase` DISABLE KEYS */;
INSERT INTO `clase` VALUES ('Introducción al diseño en 3D',NULL,'Con esta clase los alumnos seran capaces de poder\nentender y motivarse para las siguientes clases.',1,1,'image_default.png'),('Introducción a la Programación Basica',NULL,'Con esta clase los alumnos podran programar y poder así desenvolverse en el mundo de desarrollo de programas y aplicaciones.',2,2,'image_default.png'),('Mi primera aplicación con NetBeans :v',NULL,'Los niños podran crear una aplicación con Java en el IDE NetBeans bajo la POO (Programación Orientada a Objetos).',3,2,'image_default.png'),('Clase 1: Introducción a la Robotica',NULL,'Aquí va al descripción ...',4,3,'image_default.jpg'),('Base de datos',NULL,'desc para bdd',5,8,'image_default.png'),('Clases',NULL,'desc par a clases',6,8,'image_default.png'),('xd',NULL,'',8,10,'image_default.png');
/*!40000 ALTER TABLE `clase` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `credencial`
--

DROP TABLE IF EXISTS `credencial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credencial` (
  `Usuario_idUsuario` int(11) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`Usuario_idUsuario`),
  KEY `fk_Credenciales_Usuario1_idx` (`Usuario_idUsuario`),
  CONSTRAINT `fk_Credenciales_Usuario1` FOREIGN KEY (`Usuario_idUsuario`) REFERENCES `usuario` (`idUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `credencial`
--

LOCK TABLES `credencial` WRITE;
/*!40000 ALTER TABLE `credencial` DISABLE KEYS */;
INSERT INTO `credencial` VALUES (1,'playtec2019','juan.antonio@gmail.com'),(2,'playtec2019','john.timoteo@tecsup.edu.pe'),(3,'playtec2019','admin@gmail.com'),(9,'1234','ricardobq_20@hotmail.com');
/*!40000 ALTER TABLE `credencial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `curso`
--

DROP TABLE IF EXISTS `curso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `curso` (
  `idCurso` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  `imagen` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`idCurso`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `curso`
--

LOCK TABLES `curso` WRITE;
/*!40000 ALTER TABLE `curso` DISABLE KEYS */;
INSERT INTO `curso` VALUES (1,'Curso de Diseño y Desarrollo en 3D','Con este curso se podra aprender desde las\nbases hasta lo mas avanzado en Diseño y Desarrollo en 3D','image_default.png'),(2,'Curso de Programación','Aprende a programar con este curso.','image_default.png'),(3,'Curso de Robótica','Los alumnos podrán crear su primer robot utilizando tarjetas Arduino ','image_default.png'),(4,'Curso de Mecatrónica Básica','Con este curso el niño será capaz de conocer las bases para poder realizar un pequeño proyecto de mecatrónica','curso.png'),(5,'fornai','','image_default.png'),(6,'sociedad','','image_default.png'),(7,'susti','','image_default.png'),(8,'Programacion','descripcion para el curso de programmmacccciooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooon !','image_default.png'),(9,'Playbot','adassaadsadsadsadsdadsasads','image_default.png'),(10,'gg','','image_default.png');
/*!40000 ALTER TABLE `curso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `diapositivas`
--

DROP TABLE IF EXISTS `diapositivas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diapositivas` (
  `iddiapositiva` int(11) NOT NULL AUTO_INCREMENT,
  `file` varchar(300) NOT NULL,
  `clase_idclase` int(11) NOT NULL,
  `clase_curso_idCurso` int(11) NOT NULL,
  PRIMARY KEY (`iddiapositiva`,`clase_idclase`,`clase_curso_idCurso`),
  KEY `fk_diapositivas_clase1_idx` (`clase_idclase`,`clase_curso_idCurso`),
  CONSTRAINT `fk_diapositivas_clase1` FOREIGN KEY (`clase_idclase`, `clase_curso_idCurso`) REFERENCES `clase` (`idclase`, `curso_idCurso`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diapositivas`
--

LOCK TABLES `diapositivas` WRITE;
/*!40000 ALTER TABLE `diapositivas` DISABLE KEYS */;
INSERT INTO `diapositivas` VALUES (1,'introduccion-mundo-3d.jpg',1,1),(2,'demo1.png',1,1),(3,'demo2.png',1,1),(4,'robotica1.jpg',4,3),(5,'robotica2.jpg',4,3),(6,'robotica3.jpg',4,3),(7,'robotica4.jpg',4,3),(8,'robotica5.jpg',4,3),(9,'robotica6.jpg',4,3),(10,'robotica7.jpg',4,3),(11,'robotica8.jpg',4,3),(12,'robotica9.jpg',4,3),(13,'robotica10.jpg',4,3),(14,'robotica11.jpg',4,3),(15,'robotica12.jpg',4,3),(16,'robotica13.jpg',4,3);
/*!40000 ALTER TABLE `diapositivas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `idUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) NOT NULL,
  `apellido` varchar(250) NOT NULL,
  `rol` char(1) NOT NULL,
  PRIMARY KEY (`idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'Juan Antonio','Soto Cabrera','P'),(2,'John Fabian','Timoteo Torres','P'),(3,'Edwin Manuel','Baltazar Terbullino','A'),(9,'Ricardo','Berrospi Quispe','P');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario_has_curso`
--

DROP TABLE IF EXISTS `usuario_has_curso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario_has_curso` (
  `usuario_idUsuario` int(11) NOT NULL,
  `curso_idCurso` int(11) NOT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `fecha_fin` date DEFAULT NULL,
  PRIMARY KEY (`usuario_idUsuario`,`curso_idCurso`),
  KEY `fk_usuario_has_curso_curso1_idx` (`curso_idCurso`),
  KEY `fk_usuario_has_curso_usuario1_idx` (`usuario_idUsuario`),
  CONSTRAINT `fk_usuario_has_curso_curso1` FOREIGN KEY (`curso_idCurso`) REFERENCES `curso` (`idCurso`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_has_curso_usuario1` FOREIGN KEY (`usuario_idUsuario`) REFERENCES `usuario` (`idUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario_has_curso`
--

LOCK TABLES `usuario_has_curso` WRITE;
/*!40000 ALTER TABLE `usuario_has_curso` DISABLE KEYS */;
INSERT INTO `usuario_has_curso` VALUES (1,3,NULL,NULL),(1,4,NULL,NULL),(2,2,NULL,NULL);
/*!40000 ALTER TABLE `usuario_has_curso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario_tiene_cursos`
--

DROP TABLE IF EXISTS `usuario_tiene_cursos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario_tiene_cursos` (
  `Usuario_idUsuario` int(11) NOT NULL,
  `Fecha_Inicio` date NOT NULL,
  `Fecha_Fin` date NOT NULL,
  PRIMARY KEY (`Usuario_idUsuario`),
  KEY `fk_Usuario_has_Curso_Usuario_idx` (`Usuario_idUsuario`),
  CONSTRAINT `fk_Usuario_has_Curso_Usuario` FOREIGN KEY (`Usuario_idUsuario`) REFERENCES `usuario` (`idUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario_tiene_cursos`
--

LOCK TABLES `usuario_tiene_cursos` WRITE;
/*!40000 ALTER TABLE `usuario_tiene_cursos` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuario_tiene_cursos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_conectados`
--

DROP TABLE IF EXISTS `usuarios_conectados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios_conectados` (
  `idusuarios_conectados` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(250) NOT NULL,
  `apodo` varchar(100) DEFAULT NULL,
  `acceso_idacceso` int(11) NOT NULL,
  `acceso_clase_idclase` int(11) NOT NULL,
  `acceso_clase_curso_idCurso` int(11) NOT NULL,
  PRIMARY KEY (`idusuarios_conectados`,`acceso_idacceso`,`acceso_clase_idclase`,`acceso_clase_curso_idCurso`),
  KEY `fk_usuarios_conectados_acceso1_idx` (`acceso_idacceso`,`acceso_clase_idclase`,`acceso_clase_curso_idCurso`),
  CONSTRAINT `fk_usuarios_conectados_acceso1` FOREIGN KEY (`acceso_idacceso`, `acceso_clase_idclase`, `acceso_clase_curso_idCurso`) REFERENCES `acceso` (`idacceso`, `clase_idclase`, `clase_curso_idCurso`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios_conectados`
--

LOCK TABLES `usuarios_conectados` WRITE;
/*!40000 ALTER TABLE `usuarios_conectados` DISABLE KEYS */;
INSERT INTO `usuarios_conectados` VALUES (1,'Roberto Martinez Redondo','Robertito',1,1,1),(2,'Juan Redondo','Juanito',2,3,2),(3,'John Timoteo','Jhoncitoxd',2,3,2),(4,'Orlando Camavilca','Muletas de oro',2,3,2),(5,'Jesus Lanfranco Sanchez','Manito',2,3,2),(6,'Andres Anaya Tenorio','Papu',1,1,1),(7,'Jorge Flores','Jorgito',1,1,1),(8,'Grover Lazaro','Lazarillo',1,1,1),(9,'Arian Angoma Vilchez','Papa fritas',2,3,2),(15,'John Fabian Timoteo Torres','jfabiant',1,1,1),(16,'Juana Maria','Juanitaxd',1,1,1),(17,'Jorge Antonio Martienez','Hola soy Jorge',1,1,1),(18,'Gian Cristian','',1,1,1),(19,'Gabriel Checnes Ormeño','',1,1,1),(22,'Carlos Manrique Sotomayor','Cabeza de toro',1,1,1),(23,'John Castro','culonxd',1,1,1);
/*!40000 ALTER TABLE `usuarios_conectados` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-10 17:04:45
